import org.im4java.core.IMOperation;
import org.junit.Test;
import top.zdkhqh.FileUtil;
import top.zdkhqh.Im4Util;
import top.zdkhqh.ImageInfo;
import top.zdkhqh.ImageUtil;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HuangQihan
 * @date 2019/9/9
 **/
public class App {

    /**
     * 图片处理
     */
    @Test
    public void deal() {
        //图片原素材库
        String sourceFolder = "E:\\HQH\\图片素材";
        //压缩缩放后的图片库
        String targetFolder = "E:\\HQH\\图片素材\\temp";
        List<String> allFileNameList = FileUtil.getAllFileName(sourceFolder);
        //正方形
        int sideLength = 512;
        for (String name : allFileNameList) {
            String srcFilePath = sourceFolder + File.separator + name;
            ImageInfo srcFile = new ImageInfo(srcFilePath);
            String targetPath = targetFolder + File.separator + name;
            IMOperation op = new IMOperation();
            op.addImage(srcFilePath);
            op.gravity("Center");
            if (srcFile.getWidth() > srcFile.getHeight()) {
                op.extent(srcFile.getHeight(), srcFile.getHeight());
            } else {
                op.extent(srcFile.getWidth(), srcFile.getWidth());
            }
            //放大，缩小
            op.resize(sideLength, sideLength);
            op.addImage(targetPath);
            Im4Util.runConvertCmd(op, targetPath);
            System.err.println(srcFilePath + " -----------> " + targetPath);
        }
    }

    /**
     * 图片拼接
     */
    @Test
    public void splicing() {
        String tempSrc = "C:\\Users\\Administrator\\Desktop\\qtcx\\temp";
        List<String> temp = FileUtil.getAllFileName(tempSrc, 2000).stream()
                .map(e -> tempSrc + File.separator + e)
                .collect(Collectors.toList());
        ImageUtil.overlapImage("C:\\Users\\Administrator\\Desktop\\qtcx\\16.jpg",
                temp,
                "C:\\Users\\Administrator\\Desktop\\qtcx\\16_" + System.currentTimeMillis() / 1000 + ".jpg");
    }

    /**
     * 图片马赛克
     */
    @Test
    public void mosaic() {
        String src = "C:\\Users\\Administrator\\Desktop\\qtcx\\16.jpg";
        ImageInfo imageInfo = new ImageInfo(src);
        int size = 10000;
        int sideLength = (int) Math.sqrt(imageInfo.getWidth() * imageInfo.getHeight() / size);
        ImageUtil.mosaic(src,
                "C:\\Users\\Administrator\\Desktop\\qtcx\\16_" + System.currentTimeMillis() / 1000 + ".jpg",
                sideLength, size);

    }

    /**
     * 相似覆盖
     */
    @Test
    public void similarCovering() {
        String src = "C:\\Users\\Administrator\\Desktop\\qtcx\\16.jpg";
        String url = "C:\\Users\\Administrator\\Desktop\\qtcx\\temp";
        String target = "C:\\Users\\Administrator\\Desktop\\qtcx\\16_" + System.currentTimeMillis() / 1000 + ".jpg";
        int size = 5000;
        int limit = 50;
        ImageUtil.similarCovering(src, url, size, limit, target);
    }


}
