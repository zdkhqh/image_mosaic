package top.zdkhqh;

import org.im4java.core.CompositeCmd;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import java.io.File;
import java.util.List;

/**
 * @author HuangQihan
 * @date 2019/9/9
 **/
public class Im4Util {

    /**
     * imageMagic安装路径
     */
    public static final String IM4_URL = "D:\\Program Files\\ImageMagick-7.0.7-Q16";

    public static void runConvertCmd(IMOperation op, String target) {
        try {
            File file = new File(target);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }
            ConvertCmd convert = new ConvertCmd();
            if (!isLinux()) {
                convert.setSearchPath(IM4_URL);
            }
            convert.run(op);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void runCompositeCmd(IMOperation op, String target) {
        try {
            File file = new File(target);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }
            CompositeCmd convert = new CompositeCmd();
            if (!isLinux()) {
                convert.setSearchPath(IM4_URL);
            }
            convert.run(op);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 拼接图片
     *
     * @param sourceFolder 源文件夹
     * @param width        目标图片宽
     * @param height       目标图片高
     */
    public static void sitchingPictures(String sourceFolder, Integer width, Integer height) {
        //大图总像素
        long allPx = width * height;
        //千图数量
        List<String> allFileNameList = FileUtil.getAllFileName(sourceFolder);
        //平均每张图的总像素
        long avgPx = allPx / allFileNameList.size();
        //小图的宽
        int rowWidth = (int) Math.sqrt(avgPx);
        for (String name : allFileNameList) {
            String srcFilePath = sourceFolder + File.separator + name;
            ImageInfo imageInfo = new ImageInfo(srcFilePath);
        }
    }


    public static boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }


    public static void main(String[] args) {
        String srcFilePath = "C:\\Users\\Administrator\\Desktop\\qtcx\\666.jpg";
        ImageInfo srcFile = new ImageInfo(srcFilePath);
        String targetPath = "C:\\Users\\Administrator\\Desktop\\qtcx\\" + System.currentTimeMillis() + ".jpg";
        IMOperation op = new IMOperation();
        op.addImage(srcFilePath);
        op.gravity("Center");
        op.extent(srcFile.getHeight() * 3 / 4, srcFile.getHeight());
        op.addImage(targetPath);
        Im4Util.runConvertCmd(op, targetPath);
        System.err.println(srcFilePath + " -----------> " + targetPath);
    }

}
