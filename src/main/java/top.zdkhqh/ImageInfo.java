package top.zdkhqh;

import com.jfinal.kit.LogKit;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ImageInfo {
    private int height;
    private int width;

    /**
     * 获取文件信息 使用文件流
     *
     * @param picture
     * @throws Exception
     */
    public ImageInfo(File picture) throws Exception {
        FileInputStream stream = new FileInputStream(picture);
        BufferedImage sourceImg = ImageIO.read(stream);
        this.width = sourceImg.getWidth();
        this.height = sourceImg.getHeight();
        stream.close();
    }

    /**
     * 获取图片信息
     *
     * @param path 图片路径
     * @throws Exception
     */
    public ImageInfo(String path) {
        try {
            File picture = new File(path);
            BufferedImage sourceImg = ImageIO.read(new FileInputStream(picture));
            this.width = sourceImg.getWidth();
            this.height = sourceImg.getHeight();
        } catch (IOException e) {
            LogKit.error("ImageInfo " + e.getMessage());
        }
    }

    public ImageInfo(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
